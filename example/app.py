#!/usr/bin/env python
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import pprint
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['HA']

async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

notif = ''

def background_thread():
    """Example of how to send server generated events to clients."""

    while True:
        socketio.sleep(1)
        temp = {'data': 'Server generated event', 'count': 0}
        notif = 'no notification'
        FS = db['fs']
        req = FS.find_one()
        if req != None:
           FS.delete_one({})
           print 'Notification is send to clint app'
           notif = req['message']
           pprint.pprint(req)
        #req_detail_response
           if req['isReq'] != None and req['isReq'] == 0 :
               n = { 'device_code':notif['device_code'],
                     'operationCode':notif['operationCode'],
                     'state':notif['state']}
               socketio.emit('req_detail_response', {'data': n}, namespace='/test')
           else:
                socketio.emit('my_response', {'data': notif}, namespace='/test')
        # else:
        #     print 'No pending notification request'
        #
        # print ( notif )


@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)


@socketio.on('my_command', namespace='/test')
def test_message(message):
    # temp = {
    #     "dev_code" :2001,
    #     "dev_command" : 1,
    #     "msg" : "any msg",
    #     "whom" : 1
    # }
    message['type'] = 'command'
    b = db['fb'].insert(message)
    # pprint.pprint(message)


#emit('my_response',  {'data': message['data'], 'count': session['receive_count']}, broadcast=True)

@socketio.on('req_detail', namespace='/test')
def test_message(message):
    # temp = {
    #     "code" :2001,
    #     "number" : 1
    # }
    message['type'] = 'req'
    b = db['fb'].insert(message)


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    if thread is None:
        thread = socketio.start_background_task(target=background_thread)
    emit('my_response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0',port=80, debug=True)




