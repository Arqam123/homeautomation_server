# from pymongo import MongoClient
import pprint
import thread,threading,os
import pymongo
import pygame
from datetime import datetime
# import RPi.GPIO as GPIO
# Connection to Mongo DB

pygame.mixer.pre_init(44100, -16, 2, 2048)  # setup mixer to avoid sound lag
pygame.init()  # initialize pygame
answer = None

musindexes = ['1.mp3','bubbling.mp3','3.mp3','4.mp3','5.mp3','6.mp3','7.mp3','8.mp3']
muscurrentindex = 0

def songPlayThread(name):
    pygame.mixer.music.load(os.path.join('.', name))
    pygame.mixer.music.play(-1)

    # game loop
    gameloop = True
    global answer
    while gameloop:
        # indicate if music is playing
        if pygame.mixer.music.get_busy():
            print(" ... music is playing")
        else:
            print(" ... music is not playing")
        # print menu
        print("[m] to toggle music on/off")
        print("[q] to quit")

        if "q" == answer:
            # break from gameloop
            gameloop = False
        else:
            print("please press either [a], [b], [m] or [q] and [ENTER]")
        answer = None
    print("bye-bye")
    #pygame.quit()


def soundPlay(command):
    global muscurrentindex
    if 1 == command['command'] or 2 == command['command']:
        nex = -1 if command['command']==1 else 1
        muscurrentindex = (muscurrentindex + nex)%8
        muscurrentindex = (muscurrentindex*-1) if muscurrentindex < 0 else muscurrentindex
        try:
            global answer
            answer = "q"
            threading._sleep(1)
            thread.start_new_thread(songPlayThread, (musindexes[muscurrentindex],))
        except:
            print "Error: unable to start thread"

    elif 4==command['command'] or 5==command['command']:
        if pygame.mixer.music.get_busy():
            pygame.mixer.music.stop()
        else:
            pygame.mixer.music.play()
    elif 9==command['command']:
        pass

    # pygame.mixer.Sound.set_volume(0)

    pass


conn = None
try:
    conn=pymongo.MongoClient()
    print "Connected successfully!!!"
except pymongo.errors.ConnectionFailure, e:
   print "Could not connect to MongoDB: %s" % e

db = conn['HA']
FB = db.fb
FS = db.fs
user  = db.User
Record = db.Record

# GPIO.setmode(GPIO.BOARD)
# GPIO.setup(8, GPIO.OUT)
# GPIO.setup(10, GPIO.OUT)
# GPIO.setup(40, GPIO.OUT)
# GPIO.setup(12, GPIO.OUT)
# GPIO.setup(16, GPIO.OUT)
# GPIO.setup(18, GPIO.OUT)

pins = [4,10,40];
for i in pins:
    #GPIO.setmode(i,GPIO.OUT)
    print 'pint ', i , ' set as output'


def feach_Request():
    req = FB.find_one()
    isAnyRequest = False
    pprint.pprint(req)
    if req != None:
        FB.delete_one({})
        print 'find new command request'
        isAnyRequest = True
    else:
        print 'find no command request'
        isAnyRequest = False

    threading._sleep(1)
    return req,isAnyRequest


def check_Authentication(req):
    c_user = user.find_one({"user_code":req['whom']})
    if c_user == None:
        c_user = 'unknown user'
    temp = {}

    if req['type'] != None and req['type'] == 'req':
        msg =' Grant Permition to read state of device '+str(req['code'])
        isAllow = False
    else:
        msg = ' Grant Permition to change '+str(req['dev_code'])
        isAllow = True



    # if isAllow == True:
    #     msg = c_user['user_name'] + ' Grant permition to change '+ str(req['dev_code'])+' with command' + str(req['dev_command'])
    # else:
    #     msg = c_user['user_name'] + ' Request is denied to change '+ c_user['dev_code']+' with command' + c_user['dev_command']

    temp['isAllow'] = isAllow
    temp['data'] = req
    temp['notify']= msg
    return temp

def controle(pin,mode):
    if mode == 1:
        # GPIO.output(pin,GPIO.HIGH)
        print 'dev on'
    elif mode==0:
        # GPIO.output(pin,GPIO.LOW)
        print 'dev off'


def saveLog(dev_code, dev_command):
    state = 0
    if dev_command  == 1:
        state = 100
    elif dev_command  == 0:
        state = 0
    temp = {'device_code':dev_code,
            'time':datetime.now(),
            'operationCode':dev_command,
            'state':state}
    if dev_command == 2501:
        temp['operationCode'] == dev_command['command']
    Record.insert(temp)


def Approve_Request(temp):
    data = temp['data']
    dev_code = data['dev_code']
    dev_command = data['dev_command']
    saveLog(dev_code, dev_command)

    #Record.update({'device_code':dev_code},{'$set':{'state':state}});


    if dev_code == 2001:
        controle(8, dev_command)
    elif dev_code == 2002:
        controle(10, dev_command)
    elif dev_code == 2003:
        controle(40, dev_command)
    elif dev_code == 2004:
        controle(12, dev_command)
    elif dev_code == 2005:
        controle(16, dev_command)
    elif dev_code == 2006:
        controle(18, dev_command)
    elif dev_code == 2501:
        soundPlay(dev_command)

    print(str(dev_code) + ' is approve' + str(dev_code))

def Notify(temp):

    if temp['isAllow'] == False:
        c = Record.find_one({'device_code':temp['data']['code']})
        notif = {
        "isReq":0,
        "message" : c
        }
        print 'record update'
        FS.insert(notif)
    else:
        notif = {
            "isReq": 1,
            "message": temp['notify']
        }
        FS.insert(notif)


def save(msg):
    print 'log is save'
    pass

def Reset_pins():
    for i in pins:
        controle(i, 2)

Reset_pins()

while 1:
    req,isAnyRequest = feach_Request()
    if isAnyRequest:
        msg = check_Authentication(req)
        if msg['isAllow']:
            Approve_Request(msg)
        Notify(msg)
        save(msg)
    pass